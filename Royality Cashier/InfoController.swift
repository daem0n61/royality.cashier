//
//  InfoController.swift
//  Royality Cashier
//
//  Created by Дмитрий Жданов on 07/01/2019.
//  Copyright © 2019 Дмитрий Жданов. All rights reserved.
//

import UIKit

class InfoController: UIViewController {
    @IBOutlet var labels: [UILabel]!
    @IBOutlet weak var infoView: UIView!
    
    var transaction: TransactionInfo!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        infoView.layer.shadowColor = UIColor.darkGray.cgColor
        infoView.layer.shadowOpacity = 1
        infoView.layer.shadowOffset = CGSize.zero
        infoView.layer.shadowRadius = 10
        
        transaction = CurrentTransaction.shared.transaction
        labels[0].text = String(describing: Int(transaction.bonusDebit))
        labels[1].text = String(describing: Int(transaction.bonusCredit))
        labels[2].text = String(describing: Int(transaction.balance))
        labels[3].text = String(describing: Int(transaction.cashSum))
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        let alertVC = UIAlertController(title: nil, message: "Вы точно хотите отменить транзакцию?", preferredStyle: .alert)
        let no = UIAlertAction(title: "Нет", style: .cancel)
        let yes = UIAlertAction(title: "Да", style: .destructive) { (action) in
            DispatchQueue.global(qos: .userInteractive).async {
                let token = UserDefaults.standard.string(forKey: "Token")!
                let answer = API.shared.undoTransaction(token: token, transactionId: self.transaction.transactionId)
                do {
                    let parsed = try JSONDecoder().decode(UndoTransaction.self, from: answer)
                    if let error = parsed.error {
                        presentAlert(VC: self, message: error.message)
                        return
                    }
                    DispatchQueue.main.async {
                        self.navigationController?.dismiss(animated: true, completion: nil)
                    }
                } catch {
                    presentAlert(VC: self, message: "Ошибка соединения с сервером. Проверьте соединение с интернетом или повторите позже")
                    return
                }
            }
        }
        alertVC.addAction(no)
        alertVC.addAction(yes)
        self.present(alertVC, animated: true, completion: nil)
    }
}
