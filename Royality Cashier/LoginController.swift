//
//  LoginController.swift
//  Royality Cashier
//
//  Created by Дмитрий Жданов on 12/01/2019.
//  Copyright © 2019 Дмитрий Жданов. All rights reserved.
//

import UIKit

class LoginController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet var textFields: [UITextField]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UserDefaults.standard.set(true, forKey: "FirstLaunch")
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        for (index, textField) in textFields.enumerated() {
            textField.delegate = self
            textField.tag = index
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
            loginButtonTapped(0)
        }
        return false
    }
    
    @objc fileprivate func keyboardWillShow(notification: Notification) {
        let userInfo = notification.userInfo
        let keyboardSizeFrame = userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
        scrollView.contentOffset = CGPoint(x: 0, y: keyboardSizeFrame.height)
    }
    
    @objc fileprivate func keyboardWillHide() {
        scrollView.contentOffset = CGPoint.zero
    }
    
    @IBAction func tapGesture(_ sender: Any) {
        view.endEditing(true)
    }
    
    @IBAction func loginButtonTapped(_ sender: Any) {
        view.endEditing(true)
        var texts = [String]()
        for textField in textFields {
            if textField.text == String() {
                presentAlert(VC: self, message: "Заполните пустые поля")
                return
            }
            texts.append(textField.text!)
        }
        
        DispatchQueue.global(qos: .userInitiated).async {
            let answer = API.shared.login(email: texts[0], password: texts[1])
            do {
                let parsed = try JSONDecoder().decode(Login.self, from: answer)
                if let error = parsed.error {
                    presentAlert(VC: self, message: error.message)
                    return
                }
                let token = parsed.token
                UserDefaults.standard.set(token, forKey: "Token")
                DispatchQueue.main.async {
                    self.performSegue(withIdentifier: "LoginToScan", sender: nil)
                }
            } catch {
                print(error)
                presentAlert(VC: self, message: "Ошибка соединения с сервером. Проверьте соединение с интернетом или повторите позже")
                return
            }
        }
    }
    
    @IBAction func registrationButtonTapped(_ sender: Any) {
        let alertVC = UIAlertController(title: "Заявка", message: "Введите контактные данные, чтобы мы смогли с Вами связаться", preferredStyle: .alert)
        let no = UIAlertAction(title: "Отмена", style: .cancel)
        let yes = UIAlertAction(title: "Подать заявку", style: .default) { (action) in
            /*guard let name = alertVC.textFields?[0].text else {
                presentAlert(VC: self, message: "Что-то пошло не так, попробуйте еще раз")
                return
            }
            guard let contact = alertVC.textFields?[1].text else {
                presentAlert(VC: self, message: "Что-то пошло не так, попробуйте еще раз")
                return
            }*/
            self.view.endEditing(true)
            DispatchQueue.global(qos: .userInteractive).async {
                sleep(3)
                presentAlert(VC: self, message: "Заявка успешно принята")
                /*if name == "" || contact == "" {
                    presentAlert(VC: self, message: "Заполните пустые поля")
                    return
                }
                let answer = API.shared.logUserActions(description: String(describing: "\(name) - \(contact)"))
                printJSON(data: answer)
                do {
                    let parsed = try JSONDecoder().decode(SimpleAnswer.self, from: answer)
                    if let error = parsed.error {
                        presentAlert(VC: self, message: error.message)
                        return
                    }
                    presentAlert(VC: self, message: "Заявка успешно принята")
                } catch {
                    presentAlert(VC: self, message: "Ошибка соединения с сервером. Проверьте соединение с интернетом или повторите позже")
                    return
                }*/
            }
        }
        alertVC.addTextField { (textField) in
            textField.placeholder = "Имя"
            textField.keyboardType = UIKeyboardType.emailAddress
        }
        alertVC.addTextField { (textField) in
            textField.placeholder = "Почта или телефон"
            textField.keyboardType = UIKeyboardType.emailAddress
        }
        alertVC.addAction(no)
        alertVC.addAction(yes)
        self.present(alertVC, animated: true, completion: nil)
    }
}
