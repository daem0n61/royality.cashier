//
//  Utils.swift
//  Royality Cashier
//
//  Created by Дмитрий Жданов on 07/01/2019.
//  Copyright © 2019 Дмитрий Жданов. All rights reserved.
//

import UIKit

func getAppDelegate() -> AppDelegate {
    return UIApplication.shared.delegate as! AppDelegate
}

func getTutorialNavVC() -> UIViewController {
    return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TutorialNavVC")
}

func getLoginNavVC() -> UIViewController {
    return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginNavVC")
}

func getScanNavVC() -> UIViewController {
    return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ScanNavVC")
}

func getBalanceVC() -> UIViewController {
    return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BalanceVC")
}

func presentAlert(VC: UIViewController ,message: String) {
    DispatchQueue.main.async {
        let alertVC = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertVC.addAction(alertAction)
        VC.present(alertVC, animated: true, completion: nil)
    }
}

func printJSON(data: Data) {
    print(try! JSONSerialization.jsonObject(with: data, options: []))
}
