//
//  API.swift
//  Royality Cashier
//
//  Created by Дмитрий Жданов on 10/01/2019.
//  Copyright © 2019 Дмитрий Жданов. All rights reserved.
//

import Foundation

enum MethodType {
    case post
    case get
}

enum OperationType {
    case take
    case give
}

class API {
    static let shared = API()
    private var data: Data!
    //private let baseURL = "http://91.243.97.74:1155/Api"
    private let baseURL = "https://loyalty.nppltt.ru:5543/Api"
    
    func login(email: String, password: String) -> Data {
        let params = ["Email": email, "Password": password]
        configRequest(url: "Login", type: .post, token: nil, params: params)
        return data
    }
    
    func getWalletInfoById(token: String, walletId: String) -> Data {
        configRequest(url: "GetWalletInfoById?WalletId=\(walletId)", type: .get, token: token, params: nil)
        return data
    }
    
    func sendTransaction(token: String, walletId: String, price: Double, type: OperationType) -> Data {
        switch type {
        case .take:
            let params = ["TransactionId": UUID().uuidString, "WalletId": walletId, "OperType": 0, "Price": price, "IsAccumulate": false] as [String : Any]
            configRequest(url: "SendTransaction", type: .post, token: token, params: params)
        case .give:
            let params = ["TransactionId": UUID().uuidString, "WalletId": walletId, "OperType": 0, "Price": price, "IsAccumulate": true] as [String : Any]
            configRequest(url: "SendTransaction", type: .post, token: token, params: params)
        }
        return data
    }
    
    func undoTransaction(token: String, transactionId: String) -> Data {
        let params = ["TransactionId": transactionId]
        configRequest(url: "UndoTransaction", type: .post, token: token, params: params)
        return data
    }
    
    //func logUserActions(actions: [Action]) -> Data {
    func logUserActions(description: String) -> Data {
        let params = ["Actions": ["Description": description]]
        configRequest(url: "logUserActions", type: .post, token: nil, params: params)
        return data
    }
    
    fileprivate func configRequest(url: String, type: MethodType, token: String?, params: [String: Any]?) {
        guard let url = URL(string: "\(baseURL)/\(url)") else {return}
        var request = URLRequest(url: url)
        
        switch type {
        case .get:
            request.httpMethod = "Get"
        case .post:
            request.httpMethod = "Post"
        }
        
        if let token = token {
            request.setValue("Bearer " + token, forHTTPHeaderField: "Authorization")
        }
        if let params = params {
            request.addValue("application/json", forHTTPHeaderField: "Content-type")
            let httpBody = try! JSONSerialization.data(withJSONObject: params, options:[])
            request.httpBody = httpBody
        }
        
        sendRequest(request: request)
    }
    
    fileprivate func sendRequest(request: URLRequest) {
        let semaphore = DispatchSemaphore(value: 0)
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let response = response {print(response)}
            self.data = data
            semaphore.signal()
            }.resume()
        semaphore.wait()
    }
}
