//
//  Model.swift
//  Royality Cashier
//
//  Created by Дмитрий Жданов on 12/01/2019.
//  Copyright © 2019 Дмитрий Жданов. All rights reserved.
//

struct Error: Codable {
    let message: String
}

struct SimpleAnswer: Codable {
    let error: Error?
}

struct Login: Codable {
    let error: Error?
    let token: String?
}

struct GetWalletInfoById: Codable {
    let error: Error?
    let code: Int
    let balance: Double
}

struct SendTransaction: Codable {
    let error: Error?
    let code: Int
    let transactionId: String
    let balance, bonusCredit, bonusDebit, cashSum: Double
}

struct TransactionInfo: Codable {
    let transactionId: String
    let balance, bonusCredit, bonusDebit, cashSum: Double
    
    init(transaction: SendTransaction) {
        transactionId = transaction.transactionId
        balance = transaction.balance
        bonusCredit = transaction.bonusCredit
        bonusDebit = transaction.bonusDebit
        cashSum = transaction.cashSum
    }
}

class CurrentTransaction {
    static let shared = CurrentTransaction()
    var transaction: TransactionInfo!
}

struct UndoTransaction: Codable {
    let code: Int
    let error: Error?
}

struct Action: Codable {
    let description: String
    
    init(description: String) {
        self.description = description
    }
}

struct Actions: Codable {
    let actions: [Action]
    
    init(actions: [Action]) {
        self.actions = actions
    }
}
