//
//  BalanceController.swift
//  Royality Cashier
//
//  Created by Дмитрий Жданов on 07/01/2019.
//  Copyright © 2019 Дмитрий Жданов. All rights reserved.
//

import UIKit

class BalanceController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var priceTextField: UITextField!
    
    var walletId: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        priceTextField.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.global(qos: .userInteractive).async {
            let token = UserDefaults.standard.string(forKey: "Token")!
            let answer = API.shared.getWalletInfoById(token: token, walletId: self.walletId)
            do {
                let parsed = try JSONDecoder().decode(GetWalletInfoById.self, from: answer)
                if let error = parsed.error {
                    presentAlert(VC: self, message: error.message)
                    return
                }
                let balance = parsed.balance
                DispatchQueue.main.async {
                    self.balanceLabel.text = String(describing: Int(balance))
                }
            } catch {
                presentAlert(VC: self, message: "Ошибка соединения с сервером. Проверьте соединение с интернетом или повторите позже")
                return
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return false
    }
    
    @objc fileprivate func keyboardWillShow(notification: Notification) {
        let userInfo = notification.userInfo
        let keyboardSizeFrame = userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
        scrollView.contentOffset = CGPoint(x: 0, y: keyboardSizeFrame.height)
    }
    
    @objc fileprivate func keyboardWillHide() {
        scrollView.contentOffset = CGPoint.zero
    }
    
    @IBAction func tapGesture(_ sender: Any) {
        view.endEditing(true)
    }
    
    @IBAction func takeButtonTapped(_ sender: Any) {
        view.endEditing(true)
        if priceTextField.text == nil {
            presentAlert(VC: self, message: "Заполните цену товара")
            return
        }
        if let price = Double(priceTextField.text!) {
            DispatchQueue.global(qos: .userInteractive).async {
                let token = UserDefaults.standard.string(forKey: "Token")!
                let answer = API.shared.sendTransaction(token: token, walletId: self.walletId, price: price, type: .take)
                do {
                    let parsed = try JSONDecoder().decode(SendTransaction.self, from: answer)
                    if let error = parsed.error {
                        presentAlert(VC: self, message: error.message)
                        return
                    }
                    CurrentTransaction.shared.transaction = TransactionInfo(transaction: parsed)
                    DispatchQueue.main.async {
                        self.performSegue(withIdentifier: "BalanceToInfo", sender: nil)
                    }
                } catch {
                    presentAlert(VC: self, message: "Ошибка соединения с сервером. Проверьте соединение с интернетом или повторите позже")
                    return
                }
            }
        } else {
            presentAlert(VC: self, message: "Введите корректное значение в рублях")
        }
    }
    
    @IBAction func giveButtonTapped(_ sender: Any) {
        view.endEditing(true)
        if priceTextField.text == nil {
            presentAlert(VC: self, message: "Заполните цену товара")
            return
        }
        if let price = Double(priceTextField.text!) {
            DispatchQueue.global(qos: .userInteractive).async {
                let token = UserDefaults.standard.string(forKey: "Token")!
                let answer = API.shared.sendTransaction(token: token, walletId: self.walletId, price: price, type: .give)
                do {
                    let parsed = try JSONDecoder().decode(SendTransaction.self, from: answer)
                    if let error = parsed.error {
                        presentAlert(VC: self, message: error.message)
                        return
                    }
                    CurrentTransaction.shared.transaction = TransactionInfo(transaction: parsed)
                    DispatchQueue.main.async {
                        self.performSegue(withIdentifier: "BalanceToInfo", sender: nil)
                    }
                } catch {
                    presentAlert(VC: self, message: "Ошибка соединения с сервером. Проверьте соединение с интернетом или повторите позже")
                    return
                }
            }
        } else {
            presentAlert(VC: self, message: "Введите корректное значение в рублях")
        }
    }
}
