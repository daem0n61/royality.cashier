//
//  ScanController.swift
//  Royality Cashier
//
//  Created by Дмитрий Жданов on 07/01/2019.
//  Copyright © 2019 Дмитрий Жданов. All rights reserved.
//

import UIKit

import UIKit
import AVFoundation

class ScanController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    var isSearch = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let session = AVCaptureSession()
        
        if let captureDevice = AVCaptureDevice.default(for: AVMediaType.video) {
            do {
                let input = try AVCaptureDeviceInput(device: captureDevice)
                session.addInput(input)
            } catch {
                print(error)
            }
        }
        
        let output = AVCaptureMetadataOutput()
        session.addOutput(output)
        output.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        output.metadataObjectTypes = [.qr]
        
        let video = AVCaptureVideoPreviewLayer(session: session)
        video.frame = view.layer.bounds
        view.layer.addSublayer(video)
        
        session.startRunning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        isSearch = true
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if isSearch {
            if metadataObjects.count != 0 {
                if let object = metadataObjects[0] as? AVMetadataMachineReadableCodeObject {
                    if object.type == .qr {
                        isSearch = false
                        let VC = getBalanceVC() as! BalanceController
                        VC.walletId = object.stringValue
                        //presentAlert(VC: self, message: object.stringValue!)
                        navigationController?.pushViewController(VC, animated: true)
                    }
                }
            }
        }
    }
    
    @IBAction func logoutButtonTapped(_ sender: Any) {
        let alertVC = UIAlertController(title: nil, message: "Выйти из аккаунта?", preferredStyle: .alert)
        let no = UIAlertAction(title: "Нет", style: .cancel)
        let yes = UIAlertAction(title: "Да", style: .destructive) { (action) in
            UserDefaults.standard.removeObject(forKey: "Token")
            self.performSegue(withIdentifier: "ScanToLogin", sender: nil)
        }
        alertVC.addAction(no)
        alertVC.addAction(yes)
        self.present(alertVC, animated: true, completion: nil)
    }
}
